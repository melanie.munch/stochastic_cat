"""
Utilitaries to visualize the Markov Cat Problem.
"""

import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import matplotlib.patches as patches
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)

from itertools import cycle

import numpy as np

import seaborn as sns

import pyAgrum as gum
import pyAgrum.lib.dynamicBN as gdyn

import pandas as pd

def Trace(evs, trace, method, n_boxes, all_boxes, system):
    """
    Trace the probabilities of the cat position.
    """
    kao_cat = "≽^•.•^≼"
    probas = { b:np.array([]) for b in all_boxes }
    
    # Compute the probabilities for the cat position
    for T in range(len(evs)):
        evs_tr = {}
        for t in range(len(trace[:T])):
            evs_tr[ f"Position{t}" ] = [ 1 if all_boxes[k]!=trace[t] else 0 for k in range(len(all_boxes)) ]
            
        mc = gdyn.unroll2TBN(system, T+1)
        ie = gum.LazyPropagation(mc)
        ie.setEvidence(evs_tr)
        ie.makeInference()
        
        post = ie.posterior( f"Position{T}" ).tolist()
        
        for b in range(len(all_boxes)):
            probas[all_boxes[b]] = np.append( probas[all_boxes[b]], post[b]+1e-4 )
     
    # Init
    timesteps = [ f"t{t}" for t in range(len(probas["b_0"])) ]
    bottom = np.zeros(len(timesteps))
    width = 0.6  # the width of the bars: can also be len(x) sequence
    fig, ax = plt.subplots()
    
    # Colors
    cmap = plt.get_cmap('RdPu')
    ccolors = cycle( [ cmap(i/len(all_boxes)) for i in range(len(all_boxes)) ] )
    
    # Bars Coordinates
    coord_bars = dict()

    for box, proba in probas.items():
        
        # Position of Cat
        cat = [ kao_cat if evs[f"Position{t}"] == box else "" for t in range(len(evs)) ]
        
        # Trace Bar
        coord_bars[box] = ax.bar(timesteps, proba, width, label=box, bottom=bottom, 
                                 color=next(ccolors), linewidth=0.25, ec="black")
        ax.bar_label(coord_bars[box], labels=cat, label_type='center')
        bottom += proba
                
    # Highlight positions
    for t in range(len(timesteps)):
        
        # Cat
        # Position
        cat_t0 = evs[ f"Position{t}" ]
        if t+1 < len(timesteps):
            cat_t1 = evs[ f"Position{t+1}" ]
        else:
            cat_t1 = None
        
        # Draw rectangle
        x0, y0 = coord_bars[cat_t0][t].xy
        w0,h0 = coord_bars[cat_t0][t].get_width(), coord_bars[cat_t0][t].get_height()

        rect = patches.Rectangle((x0, y0), w0, h0, linewidth=1.5,
                                 fill=False)
        ax.add_patch(rect)

        # Link Rectangles
        if cat_t1 != None:
            x1,y1 = coord_bars[cat_t1][t+1].xy
            w1,h1 = coord_bars[cat_t1][t+1].get_width(), coord_bars[cat_t1][t+1].get_height()

            ax.arrow( x0+w0, y0+h0/2, x1-x0-w0, y1+h1/2-y0-h0/2,
                     head_width = 3e-2, length_includes_head=True, facecolor = "black")

        # Choices
        # Draw rectangle
        x0, y0 = coord_bars[trace[t]][t].xy
        w0,h0 = coord_bars[trace[t]][t].get_width(), coord_bars[trace[t]][t].get_height()

        rect = patches.Rectangle((x0, y0), w0, h0, linewidth=1.5,
                                 fill=False, ls="--", color="navy")
        ax.add_patch(rect)

        # Link Rectangles
        if t+1 < len(timesteps):
            x1,y1 = coord_bars[trace[t+1]][t+1].xy
            w1,h1 = coord_bars[trace[t+1]][t+1].get_width(), coord_bars[trace[t+1]][t+1].get_height()

            ax.arrow( x0+w0, y0+h0/2, x1-x0-w0, y1+h1/2-y0-h0/2,
                     head_width = 3e-2, length_includes_head=True, ls="dotted", facecolor = "navy")

        
    ax.set_title(f'Probability of cat location - {method.__name__} - {n_boxes} boxes')
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    ax.tick_params(axis='x', rotation=45)
    
    ax.set_xlabel("Time")
    ax.set_ylabel("Probability")

    plt.show()

def traceEvaluation(results, n_boxes, init=None):
    """
    Evaluate the different methods and trace a graph to represent the average number of tries.
    """   
    df = pd.DataFrame( results )
    df = pd.melt(df, value_vars=results.keys(), value_name='Number Tries')

    nb = [ len(results[k]) for k in results.keys() ][0]
    
    with sns.axes_style(style="whitegrid"):
        # Initialize the figure
        f, ax = plt.subplots()
        sns.despine(bottom=True, left=True)

        # Show each observation with a scatterplot
        sns.stripplot(
            data=df, x="Number Tries", y="variable", hue="variable",
            dodge=False, alpha=.25, zorder=1, legend=False
        )

        # Show the conditional means, aligning each pointplot in the
        # center of the strips by adjusting the width allotted to each
        # category (.8 by default) by the number of hue levels
        sns.pointplot(
            data=df, x="Number Tries", y="variable",
            join=False,  palette="dark",
            markers="d", scale=.75, errorbar=('ci', 95)
        )
        
        ax.xaxis.set_major_locator(MultipleLocator(5))
        ax.xaxis.set_minor_locator(MultipleLocator(1))
        
        ax.xaxis.grid(True, "major", linewidth=1)
        ax.xaxis.grid(True, "minor", linewidth=.25)
        
        ax.get_yaxis().get_label().set_visible(False)
        
        plt.title( f"Performance Evaluation - {n_boxes} boxes - {nb} Repetitions - Cat Init { init if init!=None else 'Random' }" )
        
        plt.show()